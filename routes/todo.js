
//Array of todo objects
var todo = []
//Push some values
todo.push({"id":1,"task":"Get Milk",
    "completed":"false"})

todo.push({"id":2,"task":"Get Eggs","completed":"true"})    


exports.getAll = function(req,res,next){
    res.json(todo)
    
    next()
}

exports.getById = function(req,res,next){
    if(req.params.id === undefined){
        res.sendStatus(503)
    }
    if(todo[req.params.id-1]=== undefined){
        res.sendStatus(503)
    }
    res.json(todo[req.params.id-1])
    next()
}

exports.create = function(req,res,next){
    
    if(req.body.task === undefined){
        res.sendStatus(403)
    }
    var temp = req.body

    temp["id"] = todo.length + 1

    todo.push(temp)

    res.json(temp)
    
    next()

}

exports.deleteById = function(req,res,next){
    if(req.params.id === undefined){
        res.sendStatus(503)
    }
    todo.splice(req.params.id-1,1)
    
    res.json(todo)
    
    next()
    
}
exports.updateById = function(req,res,next){
    
    if(req.body.completed=== undefined){
        res.sendStatus(503)
    }
    todo[req.params.id-1]["completed"] = req.body.completed

    res.json(todo[req.params.id-1])

    next()

}