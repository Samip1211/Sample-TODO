FROM node:9
ADD . /usr/src/app
WORKDIR /usr/src/app

RUN npm install -g nodemon && npm install

EXPOSE 5000

CMD ["nodemon"]
