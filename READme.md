## Introduction.

A simple CRUD API's for a todo application. Main goal for this application was to understand and perform deployments in kubernetes.

## Application.

A sample todo structure.

{
    "id":1,
    "task":"Get Milk",
    "completed":"true"
}


The application allows simple CRUD operation.

POST /api/todo : Add a new todo. Todo is passed in the body as JSON. `id` is not a requirement.

GET /api/todos : Get All todos.

GET /api/todo/:id : Get todo based on id.

PUT /api/todo/:id : Update the `completed` variable of a todo. The request contains {"completed":"true/false"} in JSON format.

DELETE /api/todo/:id : Delete a todo based on id.

## Deployment.

I used Google Cloud Platform to run my kubernetes cluster. One of the main reason to use GCP is $300 credit and not worrying about setting up the cluster to run kubernetes. GCP kibernetes engine sets up the cluster for us. And since setting up the cluster is one of the most complex task, I decided to go with GCP.

I developed a dockerfile and pushed it to Google Container Registry so that it would be easier to run the images from the kubernetes.  The following commands were executed to run the application in kubernetes.

`kubectl run app --image=gcr.io/PROJECTID/sampletodo`

`kubectl expose deployment/app --type="LoadBalancer" --port 5000`