var express = require('express')
var app = express()
const bodyParser = require("body-parser");
var todo = require('./routes/todo')

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

//Create
app.post("/api/todo",todo.create)

//ReadAll
app.get("/api/todos",todo.getAll)
//Read
app.get("/api/todo/:id",todo.getById)

//Upade
app.put("/api/todo/:id",todo.updateById)

//Delete
app.delete("/api/todo/:id",todo.deleteById)

//Listen on port
app.listen("5000",()=>{
    console.log("Listening on port 5000")
})